package solution;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SolutionVerification {
	
	static List<Person> people;
	static List<String> families;
	
	@BeforeAll
	static void setupBeforeTests() {
		people = People.getInstance().getAllMembers();
		families = People.getInstance().getMembersGroupByLastNameOrderByNumberOfMembersDescAndAgeDesc();
	}
	
	@ParameterizedTest(name="value={0}, expected={1}")
	@CsvFileSource(resources="/expected-data-from-input-file.csv")
	@DisplayName("Testing if input data is registered correctly from file")
	@Order(0)
	void testIfInputDataIsReadFromFile(int value, String expected) {
		assertEquals(expected, people.get(value).toString(),"Should be " + expected);
	}
	
	@ParameterizedTest(name="value={0}, expected={1}")
	@CsvFileSource(resources="/expected-output.csv")
	@DisplayName("Testing if results match the expected output data")
	@Order(1)
	void testIfResultsMatchDataFromOutputFile(int value, String expected) {
		assertEquals(expected, families.get(value),"Should be " + expected);
	}
}
