package solution;

import java.time.LocalDate;

public class Person {

	private String firstName;
	private String lastName;
	private LocalDate dateOfBirth;
	
	public Person() {
		
	}

	public Person(String theFirstName, String theLastName, LocalDate theDateOfBirth) {
		firstName = theFirstName;
		lastName = theLastName;
		dateOfBirth = theDateOfBirth;
	}

	public String getFirstName() {
		return firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}


	@Override
	public String toString() {
		return String.format("%s %s %s", firstName, lastName, dateOfBirth);
	}
	
	
}
