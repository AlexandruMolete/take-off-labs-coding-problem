package solution;

import java.util.List;

public class Family {

	private int numberOfMembers;
	private String name;
	private List<String> members;
	
	public Family(int membersListSize, String familyName, List<String> listOfMembers) {
		numberOfMembers = membersListSize;
		name = familyName;
		members = listOfMembers;
	}

	public int getNumberOfMembers() {
		return numberOfMembers;
	}

	public String getName() {
		return name;
	}

	public List<String> getMembers() {
		return members;
	}

	@Override
	public String toString() {
		StringBuilder membersAsString = new StringBuilder();
		members.forEach(member -> {
			membersAsString.append(member + " ");
		});
		membersAsString.deleteCharAt(membersAsString.length()-1);
		return String.format("%s: %s", name, membersAsString.toString());
	}
	
}
