package solution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class People {

	private String inputFileName = "H:\\Eclipse\\eclipse-workspace\\internship-problem\\src\\main\\resources\\input.txt";
	private List<Person> allMembers;
	
	private People() {
		
		File theInputFile = new File(inputFileName);
		
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(theInputFile))){
			
			int numberOfMembers = Integer.parseInt(bufferedReader.readLine());
			allMembers = new LinkedList<Person>();
			String inputFileData;
			
			for (int memberIndex = 0; memberIndex < numberOfMembers; memberIndex++) {
				inputFileData = bufferedReader.readLine();
				String[] personDetails = Pattern.compile(" ").split(inputFileData);
				allMembers.add(new Person(personDetails[0], personDetails[1], LocalDate.parse(personDetails[2])));
			}
			
		} catch (Exception exp) {
			System.out.println(exp.getMessage());
		}
	}
	
	private static final class SingletonHolder {
		private static final People INSTANCE = new People();
	}
	
	public static People getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public List<Person> getAllMembers() {
		return allMembers;
	}
	
	public List<String> getMembersGroupByLastNameOrderByNumberOfMembersDescAndAgeDesc(){
		
		List<String> distinctLastNames = allMembers.stream()
												.map(Person::getLastName)
												.distinct()
												.collect(Collectors.toList());
		
		List<Family> families = new LinkedList<Family>();
		
		List<String> familyMembers;
		
		for (String distinctLastName : distinctLastNames) {
			familyMembers = allMembers.stream()
									  .filter(member -> member.getLastName().equals(distinctLastName))
									  .sorted(Comparator.comparing(Person::getDateOfBirth))
									  .map(Person::getFirstName)
									  .collect(Collectors.toList());
			families.add(new Family(familyMembers.size(),distinctLastName,familyMembers));
		}
		
		return families.stream()
				       .sorted(Comparator.comparing(Family::getNumberOfMembers).reversed())
				       .map(Family::toString)
				       .collect(Collectors.toList());
		
	}
}
