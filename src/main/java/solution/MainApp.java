package solution;

import java.util.List;

public class MainApp {

	public static void main(String[] args) {
		
		List<String> families = People.getInstance().getMembersGroupByLastNameOrderByNumberOfMembersDescAndAgeDesc();
		families.forEach(System.out::println);

	}

}
